<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;

use AppBundle\Model\RandomNumberService;

class RandomNumberListener
{
    /**
     * @var RandomNumberService 
     */
    protected $randomService;
    
    public function __construct(RandomNumberService $rand_service)
    {
        $this->setRandomNumberService($rand_service);
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            // don't do anything if it's not the master request
            return;
        }
        
        if(!$this->getRandomNumberService()->hasRandomNumber()) {
            $this->getRandomNumberService()->setRandomNumber();
        }
    }
    
    /**
     * @param RandomNumberService $rand_service
     * @return $this
     */
    public function setRandomNumberService(RandomNumberService $rand_service)
    {
        $this->randomService = $rand_service;
        return $this;
    }
    
    /**
     * @return RandomNumberService
     */
    public function getRandomNumberService()
    {
        return $this->randomService;
    }
}

