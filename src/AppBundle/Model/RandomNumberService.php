<?php

namespace AppBundle\Model;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RandomNumberService
{
    /**
     *
     * @var SessionInterface 
     */
    protected $session;
    
    public function __construct(SessionInterface $session)
    {
        $this->setSession($session);
    }
    
    /**
     * @param SessionInterface $session
     * @return $this
     */
    public function setSession(SessionInterface $session)
    {
        $this->session = $session;
        return $this;
    }
    
    /**
     * @return SessionInterface
     */
    public function getSession()
    {
        return $this->session;
    }
    
    /**
     * @return boolean
     */
    public function hasRandomNumber()
    {
        return $this->getSession()->has("number");
    }
    
    /**
     * @return integer
     */
    public function setRandomNumber()
    {
        $number = random_int(0, 100);
        $this->getSession()->set("number", $number);
        return $number;
    }
    
    /**
     * @return integer
     */
    public function getRandomNumber()
    {
        return $this->getSession()->get("number");
    }
}
