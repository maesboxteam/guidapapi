<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;

class MainController extends FOSRestController
{
    /**
     * @RequestParam(name="number", requirements="\d+", description="number")
     * @ApiDoc(
     *	section="Main",
     *  resource=true,
     *  description="game",
     *  parameters={
     *      {"name"="number", "dataType"="integer", "required"=true, "description"="number"}
     *  }
     * )
     */
    public function postAction(ParamFetcher $paramFetcher)
    {
        $view = $this->view();
        
        $number = $paramFetcher->get("number");
        $rand = $this->get("guidap.random_number_service")->getRandomNumber();
        
        $result = [
            "value" => $number
        ];
        
        if ($number < $rand) {
            $result["result"] = "+";
        }
        elseif ($number > $rand) {
            $result["result"] = "-";
        }
        else {
            $result["result"] = "=";
            $this->get("guidap.random_number_service")->setRandomNumber();
        }
        
        $view->setData($result);
        
        return $this->handleView($view);
    }
}